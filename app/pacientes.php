<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pacientes extends Model
{
     /*_____________Aqui se anexan los campo de las llaves foraneas generadas____*/
     protected $primaryKey='idp';
     protected $fillable=[
     'idp','nombre','apellido1','apellido2','edad','sexo',
     'telefono','id_edo','id_mun','calle','numcalle',
     'localidad','archivo'];
     protected $date=['delete_at'];
 
}
