<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class consultas extends Model
{
     /*_____________Aqui se anexan los campo de las llaves foraneas generadas____*/
     protected $primaryKey='id_con';
     protected $fillable=[
     'id_con',
     'fecha',
     'hora',
     'id_doc',
     'idp',
     'dolor',
     'medicamento',
     'id_cita',
     'tip_cita',
     'alergias',
     'sintomas',
     'dias',
     'docis',
     'descripcion'];
     protected $date=['delete_at'];
 
}
