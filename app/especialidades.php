<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class especialidades extends Model
{
/*_____________Aqui se anexan los campo de las llaves foraneas generadas____*/
     protected $primaryKey='id_esp';
     protected $fillable=['id_esp','nombre','descripcion'];
}
