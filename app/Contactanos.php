<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactanos extends Model
{
    protected $primaryKey='idppd';
     protected $fillable=['idp','id_doc'];
}
