<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class operaciones extends Model
{
     /*_____________Aqui se anexan los campo de las llaves foraneas generadas____*/
     protected $primaryKey='id_opera';
     protected $fillable=[
     'id_opera','operacion','descripcion'];
     protected $date=['delete_at'];
 
}
