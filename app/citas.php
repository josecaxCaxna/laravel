<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class citas extends Model
{
/*_____________Aqui se anexan los campo de las llaves foraneas generadas____*/
protected $primaryKey='id_cita';
protected $fillable=['id_cita','f_agenda','f_asistencia','id_doc','idp','notac'];
}
