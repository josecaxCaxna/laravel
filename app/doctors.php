<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class doctors extends Model
{
   /*_____________Aqui se anexan los campo de las llaves foraneas generadas____*/
   protected $primaryKey='id_doc';
   protected $fillable=[
   'id_doc','nombre','apellido1','apellido2','edad','sexo',
   'id_esp','telefono','id_edo','id_mun','calle','numcalle',
   'localidad','archivo'];
}
