<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class estados extends Model
{
      protected $primaryKey='id_edo';
      protected $fillable=['id_edo','nombre'];
}
