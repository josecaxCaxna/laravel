<?php

namespace App\Http\Controllers;
use App\Contactanos;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Mail;
use Auth;

class ContactanosController extends Controller
{
    public function vercontacto()
    {
        return view('contactanos');
    }
    public function correoenviado(Request $request)
    {
        Mail::send('enviar',$request->all(),function($msj){
            $msj->to('contacto@consultoriosiglo21.com');
            $msj->subject('Usted tiene un nuevo mensaje');
            });
            $GLOBALS ['mail']=$request['correo'];
            Mail::send('reenviar',$request->all(),function($msj){
            $msj->subject('Correo de contacto');
            $msj->to($GLOBALS['mail']);
            });
            
            return view('inicio');
    }
}
