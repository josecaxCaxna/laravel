<?php

namespace App\Http\Controllers;

use App\detailDocPac;
use Illuminate\Http\Request;

class DetailDocPacController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\detailDocPac  $detailDocPac
     * @return \Illuminate\Http\Response
     */
    public function show(detailDocPac $detailDocPac)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\detailDocPac  $detailDocPac
     * @return \Illuminate\Http\Response
     */
    public function edit(detailDocPac $detailDocPac)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\detailDocPac  $detailDocPac
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, detailDocPac $detailDocPac)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\detailDocPac  $detailDocPac
     * @return \Illuminate\Http\Response
     */
    public function destroy(detailDocPac $detailDocPac)
    {
        //
    }
}
