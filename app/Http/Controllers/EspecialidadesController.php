<?php

namespace App\Http\Controllers;

use App\especialidades;

use Illuminate\Http\Request;
use Auth;


class EspecialidadesController extends Controller
{
    public function create()
    { 
/*______ este es el auto increment_______*/
	    $clavequesigue = especialidades::
        orderBy('id_esp','desc')->take(1)->get();
        $id_esps = $clavequesigue[0]->id_esp+1;
/*_______esta es la nueva variable de el modelo para aplicar en el combo__________*/
        $especialidades = especialidades::all();
        if (Auth::check()) {
        return view('altaespecialidad')->with('id_esps',$id_esps);
    }
    else
     return view('auth.login');
  
    }
    public function guardaespecialidad(Request $request)
    {
/*___________________se hacen los Request___________________*/
	    $id_esp=$request->id_esp;
		$nombre=$request->nombre;
        $descripcion=$request->descripcion;
/*___________________se validan los campos en la vista___________________*/
		$this->validate($request,[
	        'id_esp'=>'required|numeric',
 		    'nombre'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
 		    'descripcion'=>'required|regex:/^[A-Z,a-z, ]+$/',
        ]);
/*___________________OJO FALTA EL STORE EN LA DATABASE > altaespecialidad <___________________*/
	    $result=\DB::select('CALL altaespecialidad(?,?,?)',
	    [$id_esp,$nombre,$descripcion]);
	    $proceso = "Registro de nueva Especialidad";
	    $mensaje ="La Especialidad $nombre ha sido dada de alta";
	    return view ('resultado')
	    ->with('proceso',$proceso)
	    ->with('mensaje',$mensaje);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\especialidades  $especialidades
     * @return \Illuminate\Http\Response
     */
    public function show(especialidades $especialidades)
    {
        $especialidades = especialidades::orderBy('nombre')
        ->paginate('3');
        if (Auth::check()) {
        return view ('mostrarespecialidades')->with('especialidades',$especialidades);
    }
    else
     return view('auth.login');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\especialidades  $especialidades
     * @return \Illuminate\Http\Response
     */
    public function edit($id_esp)
    {
        $especialidades= especialidades::where('id_esp',$id_esp)->get();
        if (Auth::check()) {
        return view('modificaespecialidad')->with('especialidades',$especialidades[0]);
    }
    else
     return view('auth.login');
  
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\especialidades  $especialidades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id_esp=$request->id_esp;
        $nombre=$request->nombre;
        $descripcion=$request->descripcion;
        $this->validate($request,[
            'id_esp'=>'required|numeric',
            'nombre'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
            'descripcion'=>'required|regex:/^[A-Z,a-z, ]+$/',
        ]);
/*OOOOJOOOOO FALTA EL function EN LA DATABASE       >      modificaespecialidad    <         */
        $result=\DB::select('select modificaespecialidad(?,?,?)',
        [$id_esp,$nombre,$descripcion]);
        $proceso = "Modificación especialidad";
        $mensaje ="La especialidad $nombre ha sido modificada correctamente";
        return view ('resultado')
        ->with('proceso',$proceso)
        ->with('mensaje',$mensaje);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\especialidades  $especialidades
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_esp)
    {
        especialidades::find($id_esp)->delete();
        $proceso = "Eliminacion del Especialidad";
        $mensaje ="La Especialidad con clave $id_esp ha sido eliminada ";
        return view ('resultado')
        ->with('proceso',$proceso)
        ->with('mensaje',$mensaje);
    }
}
