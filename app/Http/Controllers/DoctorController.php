<?php

namespace App\Http\Controllers;

use App\Doctors;

use Illuminate\Http\Request;
use Auth;

use App\especialidades;

use App\Estados;

use App\Municipios;

class DoctorController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
/*______ este es el auto increment_______*/
   $clavequesigue = doctors::orderBy('id_doc','desc')->take(1)->get();
   $id_docs = $clavequesigue[0]->id_doc+1;
/*_______esta es la nueva variable de el modelo para aplicar en el combo__________*/
   $doctors = doctors::all();
   $especialidades = especialidades::all();
   $estados = estados::all();
   $municipios = municipios::all();
   if (Auth::check()) {
  return view('altadoctor')
  ->with('id_docs',$id_docs)
  ->with('especialidades',$especialidades)
  ->with('estados',$estados)
  ->with('municipios',$municipios);
}
else
 return view('auth.login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//     function cat(Request $request)
// {
//      $id = $request->get('id'); 
//      $municipios = municipios::where('id_edo','=',$id)->get();// la puedes abrir xD
//      return view ('municipiosform',compact('municipios')); 
// } 

    public function guardadoctor(Request $request)
    {
        $id_doc=$request->id_doc;
		$nombre=$request->nombre;
		$apellido1=$request->apellido1;
		$apellido2=$request->apellido2;
		$edad=$request->edad;
        $sexo=$request->sexo;
        $id_esp=$request->id_esp;
		$telefono=$request->telefono;
		$id_edo=$request->id_edo;
        $id_mun=$request->id_mun;
        $calle=$request->calle;
        $numcalle=$request->numcalle;
        $localidad=$request->localidad;
		$this->validate($request,[
	  'id_doc'=>'required|numeric',
 		'nombre'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
 		'apellido1'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
 		'apellido2'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
		 'edad'=>'required|integer|min:18',
         'telefono'=>'required|regex:/^[0-9]{10}$/',
         'calle'=>'required|regex:/^[A-Z,a-z, ]+$/',
		 'numcalle'=>'required|regex:/^[0-9,0-9 ]+$/',
		 'localidad'=>'required|regex:/^[A-Z,a-z, ]+$/',
		 'archivo'=>'image|mimes:gif,jpeg,jpg,png'
		]);
		$file = $request->file('img');
		if($file!= "")
		{
            $ldate = date('Ymd_His_');
            $img = $file->getClientOriginalName();
            $img2 = $ldate.$img;
            \Storage::disk('local')->put($img2, \File::get($file));
		}
		else
		{
		 $img2 = 'sinfoto.jpg';
	 }
/*OOOOJOOOOO FALTA EL STORE DE LA DATABASE         altadoctor           */
        $result=\DB::select('CALL altadoctor(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [$id_doc,$nombre,$apellido1,$apellido2,$edad,$sexo,$id_esp,$telefono,$id_edo,$id_mun,$calle,$numcalle,$localidad,$img2]);
        $proceso = "Registro de nuevo doctor";
        $mensaje ="El doctor $nombre ha sido dado de alta";
        return view ('resultado')
        ->with('proceso',$proceso)
        ->with('mensaje',$mensaje);

    }
    public function show()
    {
        $resultado=\DB::select("SELECT vd.id_doc,vd.nombre,vd.apellido1,vd.apellido2
        ,vd.edad,vd.sexo,vd.id_esp,vd.edad,vd.telefono,vd.id_edo,vd.id_mun,vd.calle
        ,vd.numcalle,vd.localidad,vd.archivo,
        a.nombre as categoria,
        c.nombre as catego,
        t.nombre as catego2
        FROM doctors AS vd
        INNER JOIN especialidades AS a ON a.id_esp = vd.id_esp
        INNER JOIN estados AS c ON c.id_edo = vd.id_edo
        INNER JOIN municipios AS t ON t.id_mun = vd.id_mun");
        if (Auth::check()) {
        return view ('mostrardoctores')
        ->with('resultado',$resultado);
    }
    else
     return view('auth.login');
    }
    public function modificadoctor($id_doc)
    {
        $doctors= doctors::where('id_doc',$id_doc)->get();
        $id_edo = $doctors[0]->id_edo;
        $nombrestado = estados::where('id_edo',$id_edo)->get();
        $estados = estados::where('id_edo','<>',$id_edo)->get();
        $id_mun = $doctors[0]->id_mun;
        $nombremunicipio = municipios::where('id_mun',$id_mun)->get();
        $municipios = municipios::where('id_mun','<>',$id_mun)->get();
        $id_esp = $doctors[0]->id_esp;
        $nombreespec = especialidades::where('id_esp',$id_esp)->get();
        $especialidades = especialidades::where('id_esp','<>',$id_esp)->get();
        if (Auth::check()) {
        return view('modificadoctor')
        ->with('doctors',$doctors[0])
        ->with('estados',$estados)
        ->with('id_edo',$id_edo)
        ->with('nombrestado',$nombrestado[0])
        ->with('municipios',$municipios)
        ->with('id_mun',$id_mun)
        ->with('nombremunicipio',$nombremunicipio[0])
        ->with('especialidades',$especialidades)
        ->with('id_esp',$id_esp)
        ->with('nombreespec',$nombreespec[0]);
    }
    else
     return view('auth.login');
    }
    public function updatedoctor(Request $request)
    {
        $id_doc=$request->id_doc;
		$nombre=$request->nombre;
		$apellido1=$request->apellido1;
		$apellido2=$request->apellido2;
		$edad=$request->edad;
        $sexo=$request->sexo;
        $id_esp=$request->id_esp;
		$telefono=$request->telefono;
		$id_edo=$request->id_edo;
        $id_mun=$request->id_mun;
        $calle=$request->calle;
        $numcalle=$request->numcalle;
        $localidad=$request->localidad;
        $img2= "";
		$this->validate($request,[
 		'nombre'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
 		'apellido1'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
 		'apellido2'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
		 'edad'=>'required|integer|min:18',
         'telefono'=>'required|regex:/^[0-9]{10}$/',
         'calle'=>'required|regex:/^[A-Z,a-z, ]+$/',
		 'numcalle'=>'required|regex:/^[0-9,0-9 ]+$/',
		 'localidad'=>'required|regex:/^[A-Z,a-z, ]+$/',
         'archivo'=>'image|mimes:gif,jpeg,jpg,png'
         ]);
         $file = $request->file('archivo');
         if($file!= "")
         {
             $ldate = date('Ymd_His_');
             $img = $file->getClientOriginalName();
             $img2 = $ldate.$img;
             \Storage::disk('local')->put($img2, \File::get($file));
         }
         else
         {
          $img2 = 'sinfoto.jpg';
      }
 /*OOOOJOOOOO FALTA EL STORE DE LA DATABASE         modificadoctor           */
        $result=\DB::select('select modificadoctor(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [$id_doc,$nombre,$apellido1,$apellido2,$edad,$sexo,$id_esp,
        $telefono,$id_edo,$id_mun,$calle,$numcalle,$localidad,$img2]);
        $proceso = "Modifica datos de doctor";
        $mensaje ="Los datos del doctor $nombre ha sido modificado";
        return view ('resultado')
        ->with('proceso',$proceso)
        ->with('mensaje',$mensaje);

    }
    public function destroy($id_doc)
    {
        doctors::find($id_doc)->delete();
		$proceso = "Eliminacion de doctor";
		$mensaje ="El doctor con clave $id_doc ha sido eliminado ";
		return view ('resultado')
		->with('proceso',$proceso)
		->with('mensaje',$mensaje);

    }
}
