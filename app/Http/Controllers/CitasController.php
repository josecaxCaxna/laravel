<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Citas;

use App\Doctors;

use App\Pacientes;

class CitasController extends Controller
{
    public function createcita()
    { 
/*______ este es el auto increment_______*/
	    $clavequesigue = citas::
        orderBy('id_cita','desc')->take(1)->get();
        $id_cita1 = $clavequesigue[0]->id_cita+1;
/*_______esta es la nueva variable de el modelo para aplicar en el combo__________*/
        $citas = citas::all();
        $doctors = doctors::all();
        $pacientes = pacientes::all();
        if (Auth::check()) {
        return view('altacita')
        ->with('id_cita1',$id_cita1)
        ->with('doctors',$doctors)
        ->with('pacientes',$pacientes);
    }
    else
     return view('auth.login');
    }
    public function guardacita(Request $request)
    {
/*___________________se hacen los Request___________________*/
	    $id_cita=$request->id_cita;
        $f_agenda=$request->f_agenda;
        $f_asistencia=$request->f_asistencia;
		$id_doc=$request->id_doc;
		$idp=$request->idp;
        $notac=$request->notac;
/*___________________se validan los campos en la vista___________________*/
		$this->validate($request,['notac'=>'required|regex:/^[A-Z,a-z, ]+$/']);
/*___________________OJO FALTA EL STORE EN LA DATABASE > altacita <___________________*/
        $result=\DB::select('CALL altacita(?,?,?,?,?,?)',
	    [$id_cita,$f_agenda,$f_asistencia,$id_doc,$idp,$notac]);
	    $proceso = "Registra una cita";
        $mensaje = "La cita $id_cita ha sido Agendada";
	    return view ('resultado')
	    ->with('proceso',$proceso)
	    ->with('mensaje',$mensaje);
    }
    public function mostrarcitas(citas $citas)
    {
        $citas = citas::orderBy('id_cita')
        ->paginate('3');
        if (Auth::check()) {
        return view ('mostrarcita')->with('citas',$citas);
    }
    else
     return view('auth.login');
    }
    public function editarcita($id_cita)
    {
        $citas= citas::where('id_cita',$id_cita)->get();
        $id_doc = $citas[0]->id_doc;
        $nombredoctor = doctors::where('id_doc',$id_doc)->get();
        $doctors = doctors::where('id_doc','<>',$id_doc)->get();
        $idp = $citas[0]->idp;
        $nombrepaciente = pacientes::where('idp',$idp)->get();
        $pacientes = pacientes::where('idp','<>',$idp)->get(); 
        if (Auth::check()) {
        return view('modificacita')
        ->with('citas',$citas[0])
        ->with('doctors',$doctors)
        ->with('id_doc',$id_doc)
        ->with('nombredoctor',$nombredoctor[0])
        ->with('pacientes',$pacientes)
        ->with('idp',$idp)
        ->with('nombrepaciente',$nombrepaciente[0]);
    }
    else
     return view('auth.login');
  
    }
    public function updatecita(Request $request)
    {
	    $id_cita=$request->id_cita;
        $f_agenda=$request->f_agenda;
        $f_asistencia=$request->f_asistencia;
		$id_doc=$request->id_doc;
		$idp=$request->idp;
        $notac=$request->notac;
/*___________________se validan los campos en la vista___________________*/
		$this->validate($request,[
	        'id_cita'=>'required|numeric',
 		    'notac'=>'required|regex:/^[A-Z,a-z, ]+$/',
        ]);
/*OOOOJOOOOO FALTA EL function EN LA DATABASE       >      modificacita    <         */
        $result=\DB::select('select modificacita(?,?,?,?,?,?)',
        [$id_cita,$f_agenda,$f_asistencia,$id_doc,$idp,$notac]);
        $proceso = "Modificación de cita";
        $mensaje ="La cita $id_cita ha sido modificada correctamente";
        return view ('resultado')
        ->with('proceso',$proceso)
        ->with('mensaje',$mensaje);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\especialidades  $especialidades
     * @return \Illuminate\Http\Response
     */
    public function borrarcita($id_cita)
    {
        citas::find($id_cita)->delete();
        $proceso = "Eliminacion del citas";
        $mensaje ="La citas con clave $id_cita ha sido eliminada ";
        return view ('resultado')
        ->with('proceso',$proceso)
        ->with('mensaje',$mensaje);
    }
}
