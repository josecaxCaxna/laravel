<?php
namespace App\Http\Controllers;
use App\pacientes;
use App\recetas;
use App\doctors;
use App\consultas;
use App\citas;
use App\detailDocPac;
use Illuminate\Http\Request;

class CitaCon extends Controller
{
       public function index()
    {
     $clavequesigue = citas::orderBy('id_cita','desc')->take(1)->get();
     $id_cita1 = $clavequesigue[0]->id_cita+1;

     $clavesi = citas::orderBy('id_cita','desc')->take(1)->get();
     $id_cita2 = $clavesi[0]->id_cita;
     $clavemas = consultas::orderBy('id_con','desc')->take(1)->get();
     $id_con1 = $clavemas[0]->id_con+1;
     $citas = citas::all(); 
     $doctors = doctors::all();
     return view('newcita')
     ->with('id_cita1',$id_cita1)
     ->with('id_cita2',$id_cita2)
     ->with('id_con1',$id_con1)
     ->with('doctors',$doctors);
    }

   function pro(Request $request)
    {
         $id = $request->get('recetas');
         $recetas = recetas::where('id_rec','=',$id)->get();        
         return view ('medicamento',compact('recetas'));
    }
    function pntte(Request $request)
    {
         $id = $request->get('id');         
     //     $pacientes = Pacientes::where('idp','=',$id)->get();
          $pacientes = \DB::select("SELECT pacientes.idp,pacientes.nombre,pacientes.apellido1,pacientes.apellido2 FROM pacientes
          INNER JOIN detail_doctors_pacientes ON pacientes.idp = detail_doctors_pacientes.idp
          INNER JOIN doctors ON detail_doctors_pacientes.id_doc = doctors.id_doc
          WHERE doctors.id_doc =$id");
     // return view ('categoria',compact('pacientes'));
          return response()->json($pacientes);
    }  
    function tablep(Request $request)
    {
         $id = $request->get('id');
         
         $tblpacientes = pacientes::where('idp','=',$id)->get();
		 
         return view ('tablapaciente',compact('tblpacientes'));
    }
    function evalCita(Request $request){
          $id = $request->get('id');
          $datosCount = \DB::select("SELECT DISTINCT COUNT(pacientes.idp) as Dato  FROM pacientes
          INNER JOIN consultas ON pacientes.idp = consultas.idp
          INNER JOIN doctors ON consultas.id_doc = doctors.id_doc
          WHERE pacientes.idp=$id");
          return response()->json($datosCount);
          // return response()->json($id);
    }
    function tblpaci(Request $request)
    {   
         $id = $request->get('id');
         $resultado = Pacientes::where('idp','=',$id)->get();
     //     $resultado = $datpaci[0]->idp;
          $resultado=\DB::select("SELECT vd.idp,vd.nombre,vd.apellido1,vd.apellido2,vd.edad,vd.sexo,
          vd.edad,vd.telefono,vd.id_edo,vd.id_mun,vd.calle,vd.numcalle,vd.localidad,vd.archivo,
          c.nombre as catego,
          t.nombre as catego2
          FROM pacientes AS vd
          INNER JOIN estados AS c ON c.id_edo = vd.id_edo
          INNER JOIN municipios AS t ON t.id_mun = vd.id_mun");
         return view ('tblpaciente',compact('resultado'));
    }  
     function seguimiento(Request $request)
    {
         $id = $request->get('id');
         
         $amacenes = pacientes::where('id_doc','=',$id)->get();
        
         return view ('select',compact('amacenes'));
     }
     public function guardaconsulta(Request $request)
     {
     $id_con=$request->id_con;
     $fecha=$request->fecha;
     $hora=$request->hora;
     $id_doc=$request->cat;
     $idp=$request->seg;
     $dolor=$request->dolor;
     $medicamento=$request->medicamento;
     $id_cita=$request->id_cita;
     $tip_cita=$request->tip_cita;
     $alergias=$request->alergias;
     $sintomas=$request->sintomas;
     $dias=$request->dias;
     $docis=$request->docis;
     $descripcion=$request->descripcion;
     $this->validate($request,[
          'medicamento'=>'required|regex:/^[A-Z,a-z, ]+$/',
          'dolor'=>'required|regex:/^[A-Z,a-z, ]+$/',
          'descripcion'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
     ]);
     $result=\DB::select('CALL altaconsulta(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
     [$id_con,$fecha,$hora,$id_doc,$idp,$dolor,$medicamento,$id_cita,$tip_cita,$alergias,$sintomas,$dias,$docis,$descripcion]);
     $proceso = "Datos de consulta obtenidos";
     $mensaje ="La consulta se registro con el folio $id_con";
     return view ('resultado')
     ->with('proceso',$proceso)
     ->with('mensaje',$mensaje);
     }
     public function mostrarconsulta()
     {
          $consultas = consultas::orderBy('id_con')
          ->paginate('10');
          return view ('mostrarconsulta')
          ->with('consultas',$consultas);
     }
}
