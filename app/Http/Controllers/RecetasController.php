<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\recetas;

class RecetasController extends Controller
{
    public function create()
    {
/*______ este es el auto increment_______*/
        $clavequesigue = recetas::
        orderBy('id_rec','desc')->take(1)->get();
        $id_recs = $clavequesigue[0]->id_rec+1;
/*_______esta es la nueva variable de el modelo para aplicar en el combo__________*/
        $recetas = recetas::all();
        if (Auth::check()) {
        return view('altareceta')->with('id_recs',$id_recs);
    }
    else
     return view('auth.login');
}
public function guardareceta(Request $request)
{
    $id_rec=$request->id_rec;
        $descripcion=$request->descripcion;
        $medicamentos=$request->medicamentos;
        $docis=$request->docis;
        $dias=$request->dias;
        $horas=$request->horas;
        
        $this->validate($request,[
      'id_rec'=>'required|numeric',
        'descripcion'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
        'medicamentos'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
        'docis'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
         'dias'=>'required|numeric',
         'horas'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
        ]);
/*OOOOJOOOOO FALTA EL STORE DE LA DATABASE         altareceta           */
        $result=\DB::select('CALL altarecetas(?,?,?,?,?,?)',
        [$id_rec,$descripcion,$medicamentos,$docis,$dias,$horas]);
        $proceso = "Registro de nueva receta";
        $mensaje ="La recetas $id_rec ha sido dada de alta";
        return view ('resultado')
        ->with('proceso',$proceso)
        ->with('mensaje',$mensaje);

    }

    public function mostrarrecetas()
    {
         $recetas = recetas::orderBy('id_rec')
            ->paginate('3');
            if (Auth::check()) {
         return view ('mostrarrecetas')
         ->with('recetas',$recetas);
        }
        else
         return view('auth.login');
    }
    public function borrarreceta($id_rec)
    {
            recetas::find($id_rec)->delete();
            $proceso = "Eliminacion de la receta";
            $mensaje ="La receta $id_rec ha sido eliminado :(";
            return view ('resultado')
            ->with('proceso',$proceso)
            ->with('mensaje',$mensaje);

    }
    public function modificareceta($id_rec)
{
        $recetas= recetas::where('id_rec',$id_rec)->get();
        return view('modificareceta')->with('recetas',$recetas[0]);
}
public function editarreceta(Request $request)
{
    $id_rec=$request->id_rec;
    $descripcion=$request->descripcion;
    $medicamentos=$request->medicamentos;
    $docis=$request->docis;
    $dias=$request->dias;
    $horas=$request->horas;
    
    $this->validate($request,[
  'id_rec'=>'required|numeric',
        'descripcion'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
        'medicamentos'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
        'docis'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
         'dias'=>'required|numeric',
         'horas'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
    ]);
        $result=\DB::select('select modificareceta(?,?,?,?,?,?)',
        [$id_rec,$descripcion,$medicamentos,$docis,$dias,$horas]);
        $proceso = "Registro de nueva receta";
        $mensaje ="La recetas $id_rec ha sido dada de alta";
        return view('resultado')
        ->with('proceso',$proceso)
        ->with('mensaje',$mensaje);;
    }
}