<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\operaciones;

class OperacionController extends Controller
{
    public function create()
    {
/*______ este es el auto increment_______*/
        $clavequesigue = operaciones::
        orderBy('id_opera','desc')->take(1)->get();
        $id_operas = $clavequesigue[0]->id_opera+1;
/*_______esta es la nueva variable de el modelo para aplicar en el combo__________*/
        $operaciones = operaciones::all();
        if (Auth::check()) {
        return view('altaoperacion')->with('id_operas',$id_operas);
}
else
 return view('auth.login');

}
public function guardaroperacion(Request $request)
{
    $id_opera=$request->id_opera;
        $operacion=$request->operacion;
        $descripcion=$request->descripcion;
        
        $this->validate($request,[
      'id_opera'=>'required|numeric',
        'operacion'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
        'descripcion'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
        ]);
/*OOOOJOOOOO FALTA EL STORE DE LA DATABASE         altareceta           */
        $result=\DB::select('CALL altaoperaciones(?,?,?)',
        [$id_opera,$operacion,$descripcion]);
        $proceso = "Registro de nueva operacion";
        $mensaje ="La operacion $operacion ha sido dada de alta";
        return view ('resultado')
        ->with('proceso',$proceso)
        ->with('mensaje',$mensaje);

    }

    public function mostraroperacion()
    {
         $operaciones = operaciones::orderBy('id_opera')
            ->paginate('3');
            if (Auth::check()) {
         return view ('mostraroperacion')
         ->with('operaciones',$operaciones);
        }
        else
         return view('auth.login');
    
    }
    public function borraroperacion($id_opera)
    {
            operaciones::find($id_opera)->delete();
            $proceso = "Eliminacion de la operacion";
            $mensaje ="La operacion $id_opera ha sido eliminado :(";
            return view ('resultado')
            ->with('proceso',$proceso)
            ->with('mensaje',$mensaje);

    }
    public function modificaroperacion($id_opera)
{
$operaciones= operaciones::where('id_opera',$id_opera)->get();
if (Auth::check()) {
        return view('modificaoperacion')->with('operaciones',$operaciones[0]);
}
else
 return view('auth.login');
}
public function editaroperacion(Request $request)
{
   $id_opera=$request->id_opera;
        $operacion=$request->operacion;
        $descripcion=$request->descripcion;
        
        $this->validate($request,[
      'id_opera'=>'required|numeric',
        'operacion'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
        'descripcion'=>'required|regex:/^[A-Z,a-z,0-9, ]+$/',
        ]);
/*OOOOJOOOOO FALTA EL STORE DE LA DATABASE         altareceta           */
        $result=\DB::select('select modificaoperacion(?,?,?)',
        [$id_opera,$operacion,$descripcion]);
        $proceso = "Registro de nueva operacion";
        $mensaje ="La operacion $operacion ha sido dada de alta";
        return view ('resultado')
        ->with('proceso',$proceso)
        ->with('mensaje',$mensaje);
    }
}