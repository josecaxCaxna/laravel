<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class municipios extends Model
{
      protected $primaryKey='id_mun';
      protected $fillable=['id_edo','id_mun','nombre'];
}
