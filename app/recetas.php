<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class recetas extends Model
{
     /*_____________Aqui se anexan los campo de las llaves foraneas generadas____*/
     protected $primaryKey='id_rec';
     protected $fillable=[
     'id_rec','descripcion','medicamentos','docis','dias','horas'];
     protected $date=['delete_at'];
 
}
