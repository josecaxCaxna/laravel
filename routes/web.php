<?php
Route::get('/', function () {
    return view('inicio');
});
/*Route::get('/altadoctor','DoctorController@index')->name('altadoctor');*/
/*________________________________Rutas para pacientes __________________________________*/
Route::get('/altapaciente','PacienteController@create')->name('altapaciente');

Route::POST('/guardapaciente', 'PacienteController@guardapaciente')->name('guardapaciente');

Route::get('/mostrarpacientes','PacienteController@mostrarpacientes')->name('mostrarpacientes');

Route::get('/borrarpaciente/{idp}','PacienteController@borrarpaciente')->name('borrarpaciente');

Route::get('/modificapaciente/{idp}','PacienteController@modificapaciente')->name('modificapaciente');

Route::POST('/edicionpaciente','PacienteController@edicionpaciente')->name('edicionpaciente');


/*________________________________Rutas para especialidad __________________________________*/

Route::get('/altaespecialidad','EspecialidadesController@create')->name('altaespecialidad');

Route::POST('/guardaespecialidad', 'EspecialidadesController@guardaespecialidad')->name('guardaespecialidad');

Route::get('/mostrarespecialidades','EspecialidadesController@show')->name('mostrarespecialidades');

Route::get('/borrarespecialidad/{id_esp}','EspecialidadesController@destroy')->name('destroy');

Route::get('/modificaespecialidad/{id_esp}','EspecialidadesController@edit')->name('edit');

Route::POST('/edicionespecialidad','EspecialidadesController@update')->name('update');

/*________________________________Rutas para Doctor __________________________________*/

Route::get('/altadoctor','DoctorController@create')->name('altadoctor');

Route::POST('/guardadoctor', 'DoctorController@guardadoctor')->name('guardadoctor');

Route::get('/mostrardoctores','DoctorController@show')->name('mostrardoctores');

Route::get('/borrardoctor/{id_doc}','DoctorController@destroy')->name('destroy');

Route::get('/modificadoctor/{id_doc}','DoctorController@modificadoctor')->name('modificadoctor');

Route::POST('/ediciondoctor','DoctorController@updatedoctor')->name('updatedoctor');

Route::get('/catPro','DoctorController@cat')->name('catPro');

/*________________________________Rutas para cita __________________________________*/

Route::get('/altacita','CitasController@createcita')->name('altacita');

Route::POST('/guardacita', 'CitasController@guardacita')->name('guardacita');

Route::get('/mostrarcitas','CitasController@mostrarcitas')->name('mostrarcitas');

Route::get('/borrarcita/{id_cita}','CitasController@borrarcita')->name('borrarcita');

Route::get('/editarcita/{id_cita}','CitasController@editarcita')->name('editarcita');

Route::POST('/updatecita','CitasController@updatecita')->name('updatecita');


/*________________________________Rutas para recetas __________________________________*/

Route::get('/altareceta','RecetasController@create')->name('altareceta');

Route::POST('/guardareceta', 'RecetasController@guardareceta')->name('guardareceta');

Route::get('/mostrarrecetas','RecetasController@mostrarrecetas')->name('mostrarrecetas');

Route::get('/borrarreceta/{id_rec}','RecetasController@borrarreceta')->name('borrarreceta');

Route::get('/modificareceta/{id_rec}','RecetasController@modificareceta')->name('modificareceta');

Route::POST('/editarreceta','RecetasController@editarreceta')->name('editarreceta');

/*________________________________Rutas para recetas __________________________________*/

Route::get('/altaoperacion','OperacionController@create')->name('altaoperacion');

Route::POST('/guardaroperacion', 'OperacionController@guardaroperacion')->name('guardaroperacion');

Route::get('/mostraroperacion','OperacionController@mostraroperacion')->name('mostraroperacion');

Route::get('/borraroperacion/{id_opera}','OperacionController@borraroperacion')->name('borraroperacion');

Route::get('/modificaroperacion/{id_opera}','OperacionController@modificaroperacion')->name('modificaroperacion');

Route::POST('/editaroperacion','OperacionController@editaroperacion')->name('editaroperacion');

Route::get('/vercontacto','ContactanosController@vercontacto')->name('vercontacto');
// pagina web
Route::get('/inicio','pagina@iniciopagina')->name('iniciopagina');

route::get('/contact','contacto@index')->name('contact');


Route::get('/index','pagina@index')->name('index');

// pagina web

Route::POST('/correoenviado','ContactanosController@correoenviado')->name('correoenviado');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Rutas para Modulo de control

Route::get('/moduloconsultas', 'CitaCon@index')->name('modul');

Route::POST('/guardaconsulta', 'CitaCon@guardaconsulta')->name('guardaconsulta');

Route::get('/historial','CitaCon@mostrarconsulta')->name('mostrarconsulta');


Route::get('/pntte', 'CitaCon@pntte')->name('pntte');

Route::get('/tablep', 'CitaCon@tablep')->name('tablep');

Route::get('/tblpaci', 'CitaCon@tblpaci')->name('tblpaci');

Route::get('/evalCita', 'CitaCon@evalCita')->name('evalCita');




