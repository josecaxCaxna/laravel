<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="{{ asset('image/logotipo.png')}}" type="image/png">
        <title>Consultorio Siglo XXI</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/linericon/style.css')}}">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/owl-carousel/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/lightbox/simpleLightbox.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/nice-select/css/nice-select.css')}}">
        <!-- main css -->
        <link rel="stylesheet" href="{{ asset('css/style.css')}}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css')}}">
    </head>
    <body>
        <!--================Header Area =================-->
        <header class="header_area">
            <div class="header_top">
                <div class="container">

                        <div class="col-sm-6 col-7">
                            <div class="top_btn d-flex justify-content-end">
                                <a href="#"></a>
                                <a href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <a class="navbar-brand logo_h"><img src="{{ asset('image/logo3.png')}}" type="logo3/png"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" style="#000000;" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                        <ul class="nav navbar-nav menu_nav ml-auto">
                            <li class="nav-item active"><a class="nav-link" href="{{route ('iniciopagina')}}">Inicio</a></li> 
                            <li class="nav-item submenu dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a class="nav-link" href="event.html"></a></li>
                                    <li class="nav-item"><a class="nav-link" href="event-details.html"></a></li>
                                </ul>
                            </li>                             
                            <li class="nav-item"><a class="nav-link" href="{{route ('vercontacto')}}">Contáctanos</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{route ('altapaciente')}}">Login</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                        </ul>
                    </div> 
                </div>
            </nav>
        </header>
        <!--================Header Area =================-->
        
        <!--================banner Area =================-->
        <section class="banner_area d-flex text-center">
            <div class="container align-self-center">
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner_content">
                            <h6>Bienvenido a: </h6>
                            <h1>Consultorio Siglo XXI</h1>
                           
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================banner Area =================-->
            
        <!--================About Area =================-->
        <section class="about_area section_gap">
            <div class="container">
                <div class="section_title text-center">
                    <h2>El mejor lugar para tus necesidades dentales</h2>
                    <p> </p>
                </div>
                <div class="row">
                    <div class="col-md-6 d_flex">
                        <div class="about_content flex">
                            <h3 class="title_color">Con los mejores cirujanos del área</h3>
                            <p>Si estas cansado de odontologos que tiene poco tacto, con malos tratamientos, y un mal resultado. Con nosotros obtendras una atención excelente, un tratamiento adecuado y a precios accesibles.

                            </p>
                           <!-- <a href="#" class="about_btn btn_hover">Read Full Story</a>-->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="{{ asset('image/s4.jpg')}}" alt="abou_img">
                    </div>
                </div>
            </div>
        </section>
        <!--================About Area =================-->
        
        <!--================Features Area =================-->
        <center>
        <section class="features_area">
            <div class="row m0">
                <div class="col-md-3 features_item" style="margin-left: 15%;">
                    <h3>Misión</h3>
                    <p>Se busca que nuestros clientes tengan la máxima satisfacción dental, es bien sabido lo poco populares que son los dentistas por lo que en Consultorio Siglo XXI queremos que nuestros clientes no sólo salgan con lindos dientes si con ganas de sonrreir y mostrarlos.</p>
                  
                </div>
                <div class="col-md-3 features_item">
                    <h3>Visión</h3>
                    <p>Se   busca que el Consultorio Siglo XXI abarque toda la zona metropolitana para poder brindar a todos un servicio dental de calidad, donde ir al dentista ya no sea un martirio, sino un placer..</p>
                    
                </div>
                <div class="col-md-3 features_item">
                    <h3>Objetivos</h3>
                    <p>Nuestro objetivo es que cualquier personas ya sea niño, niña, joven, adulto y persona mayor, pierda ese estereotipo de dentistas toscos y procedimientos dolorosos, y que cambien su forma de ver la salud bucal como algo necesario y que no tiene que ser una carga.</p>
                    
                </div>
                <!--<div class="col-md-3 features_item">
                    <h3>Spreading Light to world</h3>
                    <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from innocence and upturning of the natural.</p>
                   
                </div>
            </div>-->
        </section>
    </center>
        <!--================Features Area =================-->
        
        <!--================Sermons work Area =================-->
        <section class="sermons_work_area section_gap">
            <div class="container">
                <div class="section_title text-center">
                    <h2>Tratamientos especializados</h2>
                    <p>Nuestros cirujanos están capacitados para cualquier tipo de tratamiento dental</p>
                </div>
                <div class="sermons_slider owl-carousel">
                    <div class="item row">
                        <div class="col-lg-8">
                            <div class="sermons_image">
                                <img src="{{ asset('image/s6.jpg')}}" alt="">
                                <p>Estudio, prevención , diagnostico y tratamiento de las anomalias de la forma y función de las estructuras dentomaxilofaciales.</p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="sermons_content">
                                <h2 class="title_color">Ortodoncia</h2>
                                <ul class="list_style sermons_category">
                                    <li><i class="lnr lnr-user"></i><span>Tipo de material dental: </span><a href="#"> metal, plastico</a></li>
                                    <li><i class="lnr lnr-database"></i><span>Precio: </span> 20,000 $</li>
                                    <li><i class="lnr lnr-calendar-full"></i><span>Duración del tratamiento:</span> 5-30 meses</li>
                                </ul>
                                <!--<a href="#" class="btn_hover">View More Details</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="item row">
                        <div class="col-lg-8">
                            <div class="sermons_image">
                                <img src="{{ asset('image/s3.jpg')}}"  alt="">
                                <p>Su duración dependerá de las características de cada persona y de sus hábitos de vida. Lo normal es que los resultados de un blanqueamiento dental perduren al menos 12 meses, pero con tratamientos complementarios, como el refuerzo de blanqueamiento dental los resultados pueden mantenerse a más largo plazo</p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="sermons_content">
                                <h3 class="title_color">Blanqueamiento dental</h3>
                                <ul class="list_style sermons_category">
                                    <li><i class="lnr lnr-user"></i><span>Tipo de material dental: </span><a href="#">Ninguno </a></li>
                                    <li><i class="lnr lnr-database"></i><span>precio: </span> 2999$</li>
                                    <li><i class="lnr lnr-calendar-full"></i><span>Duración del tratamieto: </span>12 meses</li>
                                </ul>
                             <!--   <a href="#" class="btn_hover">View More Details</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="item row">
                        <div class="col-lg-8">
                            <div class="sermons_image">
                                <img src="{{ asset('image/s5.jpg')}}" alt="">
                                <p>No solo tiene un impacto estético muy importante cuando nos relacionamos con los demás, sino que, además, los problemas de salud en esta zona pueden afectar a nuestro bienestar global.</p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="sermons_content">
                                <h3 class="title_color">Implante dental</h3>
                                <ul class="list_style sermons_category">
                                    <li><i class="lnr lnr-user"></i><span>Tipo de material dental: </span><a href="#">metal, plastico</a></li>
                                    <li><i class="lnr lnr-database"></i><span>Precio: </span>Depende el numero de implantes</li>
                                    <li><i class="lnr lnr-calendar-full"></i><span>Duración  del trataiento:</span>Permanente </li>
                                </ul>
                             <!--   <a href="#" class="btn_hover btn_hover_two">View More Details</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================Sermons work Area=================-->
        <!--================Event Blog Area=================-->
        <section class="event_blog_area section_gap">
            <div class="container">
                <div class="section_title text-center">
                    <h2>Conoce las instalaciones</h2>
                    <p></p>
                </div>
                <div class="row">
                    <div class="col-md-4">         
                        <div class="event_post">
                            <img src="{{ asset('image/c2.jpeg')}}" alt="">
                            <a href="#"><h2 class="event_title">Instalaciones de calidad</h2></a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="event_post">
                            <img src="{{ asset('image/c1.jpeg')}}" alt="">
                            <a href="#"><h2 class="event_title">Para la atención de los niños</h2></a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="event_post">
                            <img src="{{ asset('image/c5.png')}}" alt="">
                            <a href="#"><h2 class="event_title">Contamos con una Farmacia</h2></a>
                        </div>
                    </div>
                </div>
                <br>
                {{-- style="margin-left:30%;" --}}
            </div>
            <br><br><br>
            <div class="container">
                <div class="section_title text-center"><h2>Conoce nuestra ubicación</h2></div>
                <iframe  src="{{ asset('https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3766.5214730110365!2d-99.534803585096!3d19.25967598698282!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cdf4c54aa51593%3A0xb9442886b1af3d40!2sFarmacia%20Siglo%20XXI!5e0!3m2!1ses-419!2smx!4v1573717074630!5m2!1ses-419!2smx')}}" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>

        </section>

        <!--================Blog Area=================-->
        
        
        <!--================ start footer Area  =================-->	
        <footer class="footer-area section_gap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3  col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Consultorio dental</h6>
                            <p>Si usted tiene dudas adicionales puede visitarnos y con mucho gusto se le atendera

                            </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title"></h6>
                            <div class="row">
                                <div class="col-4">
                                   <!--- <ul class="list_style">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">Feature</a></li>
                                        <li><a href="#">Services</a></li>
                                        <li><a href="#">Portfolio</a></li>
                                    </ul>
                                </div>
                                <div class="col-4">
                                    <ul class="list_style">
                                        <li><a href="#">Team</a></li>
                                        <li><a href="#">Pricing</a></li>
                                        <li><a href="#">Blog</a></li>
                                        <li><a href="#">Contact</a></li>
                                    </ul>-->
                                </div>										
                            </div>							
                        </div>
                    </div>							
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Dirección  </h6>
                            <p>Farmacia Siglo XXI Calle Independencia 606, San Francisco, 52100 San Mateo Atenco, Méx. </p>		
                            <div id="mc_embed_signup">
                                <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="subscribe_form relative">
                                    <div class="input-group d-flex flex-row">
                                        <!--<input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
                                        <button class="btn sub-btn"><span class="lnr lnr-location"></span></button>	-->	
                                    </div>									
                                    <div class="mt-10 info"></div>
                                </form>
                            </div>
                        </div>
                    </div>

                <div class="border_line"></div>
            </div>
        </footer>
		<!--================ End footer Area  =================-->
        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ asset('js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{ asset('js/popper.js')}}"></script>
        <script src="{{ asset('js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('vendors/owl-carousel/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('js/jquery.ajaxchimp.min.js')}}"></script>
        <script src="{{ asset('js/mail-script.js')}}"></script>
        <script src="{{ asset('js/stellar.js')}}"></script>
        <script src="{{ asset('vendors/lightbox/simpleLightbox.min.js')}}"></script>
        <script src="{{ asset('vendors/flipclock/timer.js')}}"></script>
        <script src="{{ asset('vendors/nice-select/js/jquery.nice-select.min.js')}}"></script>
        <script src="{{ asset('js/custom.js')}}"></script>
    </body>
</html>