<!DOCTYPE html>
<html>
<head>
	<title>Modulo Consulta</title>
  <script src="{{asset('jquery-3.4.1.min.js')}}"></script>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  {{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> --}}
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> --}}
</head>
<style>
</style>
<body>
  <h1>Modulo de consultas</h1>
<script>
    var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    var f=new Date();
    document.write("Fecha: " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
</script> 
  <label style="margin-left:60%;"><p>La clave de la cita es: 00{{$id_cita1}}</p></label>
{{--------- FORMULARIO PARA EL MODULO DE CONTROL OLIVER---------------------------------------------------------------  --}}
{{-- action = "{{route ('guardacons')}}" --}}
<form action = "{{route ('guardaconsulta')}}" method = "POST"  enctype='multipart/form-data'>
    {{csrf_field()}}
   <table class="table">
<!------------------------Cambio de input--------------------------------->
    <tr><td>Fecha:</td>
    <td><input class="form-control" type='text' value="{{$id_con1}}" readonly></td></tr>
<!------------------------Cambio de input--------------------------------->
    <tr><td>Fecha:</td>
    <td><input class="form-control" type='date' name ='fecha' placeholder="La fecha es requerida" id='fecha'></td></tr>
<!------------------------Cambio de input--------------------------------->
    <tr><td>
      @if($errors->first('hora'))
          <i> {{ $errors->first('hora') }} <br></i>
        @endif
      Nombre:</td><td><input class="form-control" type = 'time' name = 'hora' placeholder="La hora es requerida" id="hora" value="{{old('hora')}}"></td></tr>
<!------------------------Cambio de input--------------------------------->
<tr><td>
  Doctor:</td><td>
  <select name = 'cat' id= 'cat'>
  @foreach($doctors as $doctor)
      <option value="{{$doctor->id_doc}}">{{$doctor->nombre}}</option>
  @endforeach
</select></td></tr>
<!------------------------Cambio de input--------------------------------->
<tr> <td>
  Paciente:
</td><td>
  <div id="pro">
      <select disabled> 
          <option>Selecciona</option>
      </select>
  </div></td></tr>
  <!------------------------Cambio de input--------------------------------->
<tr> <td>
  Tipo de cita:
</td><td>
    <input type="radio" name="tiseg" value="1" checked>Inicial
    <input type="radio" name="tiseg" value="2" >Seguimiento
  </td></tr></div>
  <!------------------------Cambio de input--------------------------------->
  <tr id="alerg">
    <div>
    <td>Alergias: </td>
    <td>
      @if($errors->first('alergia'))
      <i> {{ $errors->first('alergia') }}<br></i>
      @endif
      <input class="form-control" type="text" placeholder="Alergias" value="{{old('alergia')}}"></td>
  </div>
  </tr>
    <!------------------------Cambio de input--------------------------------->
  <tr id="sitma">
    <div>
    <td>Sintomas: </td>
    <td>
      @if($errors->first('stom'))
      <i> {{ $errors->first('stom') }}<br></i>
      @endif
      <input class="form-control" type="text" placeholder="Sintoma" value="{{old('stom')}}"></td>
  </div>
  </tr>
    <!------------------------Cambio de input--------------------------------->
    <tr>
      <div id="dolor">
      <td>Tipo de dolor: </td>
      <td>
        @if($errors->first('dolor'))
        <i> {{ $errors->first('dolor') }}<br></i>
        @endif
        <input class="form-control" type="text" placeholder="Molestias" value="{{old('dolor')}}"></td>
    </div>
    </tr>
    <!------------------------Cambio de input--------------------------------->
        <tr>
          <div id="medica">
          <td>Medicamento: </td>
          <td>
            @if($errors->first('medi'))
            <i> {{ $errors->first('medi') }}<br></i>
            @endif
            <input class="form-control" type="text" placeholder="Medicamento" value="{{old('medi')}}"></td>
        </div>
        </tr>
      <!------------------------Cambio de input--------------------------------->
          <tr>
            <div id="docss">
            <td>Docis: </td>
            <td>
              @if($errors->first('docis'))
              <i> {{ $errors->first('docis')}}<br></i>
              @endif
              <input class="form-control" type="number" min="0" max="90" placeholder="0" value="{{old('docis')}}"></td>
          </div>
          </tr>
            <!------------------------Cambio de input--------------------------------->
    <tr>
      <div id="diasmed">
      <td>Dias: </td>
      <td>
        @if($errors->first('dias'))
        <i> {{ $errors->first('dias') }}<br></i>
        @endif
        <input class="form-control" type="number" min="0" max="15" placeholder="0" value="{{old('dias')}}"></td>
    </div>
    </tr>
    <!------------------------Cambio de input--------------------------------->
      <tr>
        <div id="dscrip">
        <td>Descripción: </td>
        <td>
          @if($errors->first('desc'))
          <i> {{ $errors->first('desc') }}<br></i>
          @endif
          <textarea class="form-control" style="width:80%;" type="textarea" placeholder="Descripción..." value="{{old('desc')}}"></textarea></td>
      </div>
      </tr>
      <!------------------------Cambio de input--------------------------------->
    <tr><td colspan=2><center><input class="btn btn-lg btn-info" type ='submit' value = 'Guardar'></center></td></tr>
   </form>
</body>
  <script>
    $(document).ready(function(){
      $("#seguimiento").hide();
      $("#pro").click(function(){
        $("#seguimiento").show();
      });
    });
  </script>
  <script>
      $(document).ready(function(){
      $("#sitma").show();
      $("#alerg").show();
      $("#dolor").show();
      $("#medica").show();
      $("#docss").show();
      // $("#tabseg").hide();
      $("input[name=tiseg]").on('click',function(){
      switch($('input:radio[name=tiseg]:checked').val()){
      case '1':
      $("#sitma").show();
      $("#alerg").show();
      $("#dolor").hide();
      $("#medica").hide();
      $("#docss").hide();
      alert("uno si");
      break;
      case '2':
      $("#sitma").hide();
      $("#alerg").hide();
      $("#dolor").show();
      $("#medica").show();
      $("#docss").show();
      alert("dos si");
      break;
      }		
     });
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#ss1").click(function() {
        $("#tabseg").load('{{route('tblpaci')}}' + '?id='  +this.options[this.selectedIndex].value) ;
      });
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#cat").click(function() {
        $("#pro").load('{{route('pntte')}}' + '?id='  +this.options[this.selectedIndex].value) ;
        });       
      });
  </script>
    <script>
      $(document).ready(function(){
        $("#save").click(function() {
          alert("Por el momento se esta desarrollando este modulo");
        }); 
      });
    </script>
</html>