<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="{{ asset('image/logotipo.png')}}" type="image/png">
        <title>Consultorio Siglo XXI</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/linericon/style.css')}}">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/owl-carousel/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/lightbox/simpleLightbox.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/nice-select/css/nice-select.css')}}">
        <!-- main css -->
        <link rel="stylesheet" href="{{ asset('css/style.css')}}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css')}}">
    </head>
    <body>
        <!--================Header Area =================-->
       <!--================Header Area =================-->
       <header class="header_area">
        <div class="header_top">
            <div class="container">

                    <div class="col-sm-6 col-7">
                        <div class="top_btn d-flex justify-content-end">
                            <a href="#"></a>
                            <a href="#"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h"><img src="{{ asset('image/logo3.png')}}" type="logo3/png"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" style="#000000;" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        <li class="nav-item active"><a class="nav-link" href="{{route ('iniciopagina')}}">Inicio</a></li> 
                        <li class="nav-item submenu dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item"><a class="nav-link" href="event.html"></a></li>
                                <li class="nav-item"><a class="nav-link" href="event-details.html"></a></li>
                            </ul>
                        </li>                             
                        <li class="nav-item"><a class="nav-link" href="{{route ('vercontacto')}}">Contáctanos</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route ('altapaciente')}}">Login</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                    </ul>
                </div> 
            </div>
        </nav>
    </header>
        <!--================Header Area =================-->
        
        <!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area br_image">
            <div class="container">
                <div class="page-cover text-center">
                    <h2 class="page-cover-tittle">Consultorio Siglo XXI</h2>
                    <ol class="breadcrumb">
                        <!--<li><a href="index.html">Home</a></li>
                        <li class="active">Contact Us</li>-->
                    </ol>
                </div>
            </div>
        </section>
        <!--================Breadcrumb Area =================-->
        <!--================Contact Area =================-->
        <section class="contact_area section_gap">
            <div class="container">
                <form action = "{{route ('correoenviado')}}" method = "POST">
                    {{csrf_field()}}
                    <table class="table">
                    <tr>
                    <td><h3>Correo:</h3><input id="correo" name="correo" class="form-control" type="text" placeholder="Ingresa tu correo" required></td>
                    </tr>
                    <tr>
                    <td><h3>Telefono:</h3><input id="telefono" name="telefono" class="form-control" type="text" placeholder="Ingresa tu teléfono" maxlength="10" minlength="10" required></td>
                    </tr>
                    <tr>
                    <td><h3>Mensaje:</h3> <input id="mensaje" name="mensaje" class="form-control" type="text" placeholder="Anota alguna duda o aclaración" required></td>
                    </tr>
                    <tr>
                    <td><input onclick="envios()"  type="submit" style="margin-left:50%;"  class="btn btn-lg btn-info" value="ENVIAR"></td>
                    </tr>
                    </table>
                </form>
            </div>
        </section>
        <script>
        function envios()
        {
       alert("Enviar correo a Consultorio siglo XXI");
        }
        </script>
        <!--================Contact Area =================-->
        
        <!--================ start footer Area  =================-->	
        <footer class="footer-area section_gap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3  col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Consultorio dental</h6>
                            <p>Si usted tiene dudas adicionales puede visitarnos y con mucho gusto se le atendera </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title"></h6>
                            <div class="row">
                                <div class="col-4">
                                </div>										
                            </div>							
                        </div>
                    </div>							
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Dirección</h6>
                            <p>Farmacia Siglo XXI Calle Independencia 606, San Francisco, 52100 San Mateo Atenco, Méx. </p>		
                            <div id="mc_embed_signup">
                                <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="subscribe_form relative">
                                    <div class="input-group d-flex flex-row">
                                       <!--- <input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
                                        <button class="btn sub-btn"><span class="lnr lnr-location"></span></button>	-->	
                                    </div>									
                                    <div class="mt-10 info"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                    </div>						
                </div>
                <div class="border_line"></div>
            </div>
        </footer>
		<!--================ End footer Area  =================-->
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ asset('js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{ asset('js/popper.js')}}"></script>
        <script src="{{ asset('js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('vendors/owl-carousel/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('js/jquery.ajaxchimp.min.js')}}"></script>
        <script src="{{ asset('js/mail-script.js')}}"></script>
        <script src="{{ asset('js/stellar.js')}}"></script>
        <script src="{{ asset('vendors/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
        <script src="{{ asset('vendors/isotope/isotope-min.js')}}"></script>
        <script src="{{ asset('js/stellar.js')}}"></script>
        <script src="{{ asset('vendors/lightbox/simpleLightbox.min.js')}}"></script>
        <script src="{{ asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE')}}"></script>
        <script src="{{ asset('js/gmaps.min.js')}}"></script>
        <script src="{{ asset('js/jquery.form.js')}}"></script>
        <script src="{{ asset('js/jquery.validate.min.js')}}"></script>
        <script src="{{ asset('vendors/nice-select/js/jquery.nice-select.min.js')}}"></script>
        <script src="{{ asset('js/contact.js')}}"></script>
        <script src="{{ asset('js/custom.js')}}"></script>
    </body>
</html>