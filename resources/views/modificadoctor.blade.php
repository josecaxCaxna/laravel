<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Consultorio Siglo XXI</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" rel="stylesheet" href="{{ asset('images/favicon1.png')}}">
    <link rel="shortcut icon" rel="stylesheet" href="{{ asset('images/favicon1.png')}}">
    <link rel="stylesheet" href="{{ asset('https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css')}}">
    <link rel="stylesheet" href="{{ asset('https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css')}}">
    <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/chosen/chosen.min.css')}}">
    <link rel='stylesheet' href="{{ asset('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800')}}">
</head>
<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                    <a href="{{route ('index')}}"><i class="menu-icon fa fa-home"></i>HOME</a>
                </li>
                    <li class="menu-title">Consultorio Siglo XXI</li>
                    <li>
                      <a href="{{route ('modul')}}"><i class="menu-icon fa fa-laptop"></i>CONSULTAS</a>
                  </li>                    <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user-md"></i>Doctores</a>
                     <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-edit"></i><a href="{{route ('altadoctor')}}">Registro</a></li>
                       <li><i class="fa fa-th-list"></i><a href="{{route ('mostrardoctores')}}">Consulta</a></li>
                     </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Pacientes</a>
                     <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-edit"></i><a href="{{route ('altapaciente')}}">Registro</a></li>
                       <li><i class="fa fa-th-list"></i><a href="{{route ('mostrarpacientes')}}">Consulta</a></li>
                     </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Citas</a>
                     <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-edit"></i><a href="{{route ('altacita')}}">Registro</a></li>
                       <li><i class="fa fa-th-list"></i><a href="{{route ('mostrarcitas')}}">Consulta</a></li>
                     </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-list-alt"></i>Recetas</a>
                     <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-edit"></i><a href="{{route ('altareceta')}}">Registro</a></li>
                       <li><i class="fa fa-th-list"></i><a href="{{route ('mostrarrecetas')}}">Consulta</a></li>
                     </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-h-square"></i>Operaciones</a>
                     <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-edit"></i><a href="{{route ('altaoperacion')}}">Registro</a></li>
                       <li><i class="fa fa-th-list"></i><a href="{{route ('mostraroperacion')}}">Consulta</a></li>
                     </ul>
                    </li>
                     <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bookmark"></i>Especialidades</a>
                     <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-edit"></i><a href="{{route ('altaespecialidad')}}">Registro</a></li>
                       <li><i class="fa fa-th-list"></i><a href="{{route ('mostrarespecialidades')}}">Consulta</a></li>
                     </ul>
                    </li>
                    <li>
                        <a href="{{route ('mostrarconsulta')}}"><i class="menu-icon fa fa-file"></i>Historial de consultas</a>
                    </li> 
                </ul>
            </div>
        </nav>
    </aside>
<!-----------------------------------Termina Barra del menu de Navegacion------------------------------->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="./"><img src="{{ asset('images/logo.png')}}" alt="Logo"></a>
                    <a class="navbar-brand hidden" href="./"><img src="{{ asset('images/logo2.png')}}" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="{{ asset('images/admin.jpg')}}" alt="User Avatar">
                        </a>
                        <div class="user-menu dropdown-menu">
                             <a class="nav-link"><i class="fa fa-user"></i>{{ Auth::user()->name }}</a>
                            <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                             <i class="fa fa-power-off"></i>{{ __('Cerrar Sesión') }}</a>
                             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </header>
        <!-- /#header -->
        <!-- Content -->
<!-------------COMIENZA ENCABEZADO DE TITULO DE FORMULARIO------------------------------>
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <div class="row">
                                    <table>
                                        <tr>
                                        <th><i class="menu-icon fa fa-edit"></i></th><th style="margin-right:25%;"></th>
                                        <th><div class="box-title"><h2>Modifica de doctores</h2>
                                        </div></th>
                                        </tr>
                                    </table>
                                </div>                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-------------TERMINA ENCABEZADO DE TITULO DE FORMULARIO------------------------------>
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
<!----------------------------------------------------------CONTENEDOR DE LAS LOS FORMULARIOS Y TABLAS---------------------------------------->
                            <div class="orders">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="table-stats order-table ov-h">
<!----------------------------------------------------------/**/*/START THE FORM*/*/*/---------------------------------------->
    <form action = "{{route ('updatedoctor')}}" method = "POST" enctype='multipart/form-data'>
        {{csrf_field()}}
       <table class="table">
<!------------------------Cambio de input--------------------------------->
        <tr><td>Clave de Doctor:</td>
        <td><input class="form-control"type='text' name ='id_doc' id='clave' value="{{$doctors->id_doc}}" readonly='readonly'></td></tr>
<!------------------------Cambio de input--------------------------------->
        <tr><td>
          @if($errors->first('nombre'))
              <i> {{ $errors->first('nombre') }} <br></i>
            @endif
          Nombre:</td><td><input class="form-control"type = 'text' name = 'nombre' id="nombre"  value="{{$doctors->nombre}}"></td></tr>
<!------------------------Cambio de input--------------------------------->    
        <tr><td>
          @if($errors->first('apellido1'))
              <i> {{ $errors->first('apellido1') }}<br></i>
            @endif
            Apellido Paterno:</td>
            <td><input class="form-control"type = 'text' name = 'apellido1'id="apellido1"  value="{{$doctors->apellido1}}"></td></tr>
<!------------------------Cambio de input--------------------------------->
            <tr><td>
          @if($errors->first('apellido2'))
             <i> {{ $errors->first('apellido2') }}<br> </i>
           @endif
          Apellido Materno:</td>
          <td><input class="form-control"type = 'text' name = 'apellido2' id="apellido2"  value="{{$doctors->apellido2}}"></td></tr>
<!------------------------Cambio de input--------------------------------->
          <tr><td>
          @if($errors->first('edad'))
             <i> {{ $errors->first('edad') }}<br></i>
           @endif
          Edad:</td>
          <td><input class="form-control" type= 'text' name = 'edad' id="edad"  value="{{$doctors->edad}}"></td></tr>
<!------------------------Cambio de input--------------------------------->
<tr><td>Sexo:</td>
<td><input type='radio' name='sexo' value='M' checked>Masculino  <input type='radio' name='sexo' value='F'>Femenino</td></tr>
<!------------------------Cambio de input--------------------------------->
<tr><td>Especialidad:</td><td> <select class="form-control" name = 'id_esp'>
    @foreach($especialidades as $desp)
  <option value = '{{$desp->id_esp}}'>{{$desp->nombre}}</option>
    @endforeach
  </select></td></tr>
<!------------------------Cambio de input--------------------------------->
        <tr><td>
          @if($errors->first('telefono'))
             <i> {{ $errors->first('telefono') }}<br></i>
           @endif
         Telefono:</td><td><input class="form-control" type = 'text' name = 'telefono' id="telefono"  value="{{$doctors->telefono}}"></td></tr>
<!------------------------Cambio de input--------------------------------->
<tr><td>Estado:</td><td> <select class="form-control" name = 'id_edo'>
        @foreach($estados as $edo)
      <option value = '{{$edo->id_edo}}'>{{$edo->nombre}}</option>
        @endforeach
      </select></td></tr>
<!------------------------Cambio de input--------------------------------->
     <tr><td>Municipio:</td><td> <select class="form-control" name = 'id_mun'>
       @foreach($municipios as $mun)
     <option value = '{{$mun->id_mun}}'>{{$mun->nombre}}</option>
     @endforeach
     </select></td></tr>
        <!------------------------Cambio de input--------------------------------->
       <tr><td>
          @if($errors->first('calle'))
             <i> {{ $errors->first('calle') }}<br></i>
           @endif
         Calle:</td><td><input class="form-control" type = 'text' name = 'calle' id="calle"  value="{{$doctors->calle}}"></td></tr>
<!------------------------Cambio de input--------------------------------->
         <tr><td>
            @if($errors->first('numcalle'))
               <i> {{ $errors->first('numcalle') }}<br></i>
             @endif
            Numero de calle:</td>
            <td><input class="form-control" type = 'text' name = 'numcalle' id="numcalle"  value="{{$doctors->numcalle}}"></td></tr>
<!------------------------Cambio de input--------------------------------->
        <tr><td>
          @if($errors->first('localidad'))
             <i> {{ $errors->first('localidad') }}<br></i>
           @endif
          Localidad:</td>
          <td><input class="form-control" type = 'text' name = 'localidad' id="localidad"  value="{{$doctors->localidad}}"></td></tr>
<!------------------------Cambio de input--------------------------------->
<tr><td>Foto Actual </td>
    <td>
    <img src = "{{asset('storage/'.$doctors->archivo)}}" height =70 width=70>
    </td> </tr>
<tr><td>Seleccionar foto</td>
<td  colspan=2><input class="form-control" type ='file' name = 'archivo'></td></tr>
<tr><td colspan=2><center><input class="btn btn-lg btn-info" type ='submit' value = 'Guardar'></center></td></tr>
    </form>

<!----------------------------------------------------------/**/*/TERMINA FORM*/*/*/---------------------------------------->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<!----------------------------------------------------------/**/*/*/*/*/---------------------------------------->
    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="assets/js/main.js"></script>
<!------------------------------------------------------ Borrad de aqui ----------------------------------------------- -->
</body>
</html>
