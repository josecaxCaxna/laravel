<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Consultorio Siglo XXI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" rel="stylesheet" href="{{ asset('images/favicon1.png')}}">
    <link rel="shortcut icon" rel="stylesheet" href="{{ asset('images/favicon1.png')}}">
    <link rel="stylesheet" href="{{ asset('https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css')}}">
    <link rel="stylesheet" href="{{ asset('https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css')}}">
    <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/chosen/chosen.min.css')}}">
    <link rel='stylesheet' href="{{ asset('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800')}}">
      <script src="{{asset('jquery-3.4.1.min.js')}}"></script>
</head>
<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                  <li>
                    <a href="{{route ('index')}}"><i class="menu-icon fa fa-home"></i>HOME</a>
                </li>
                    <li class="menu-title">Consultorio Siglo XXI</li>
                    <li class="active">
                      <a href="{{route ('modul')}}"><i class="menu-icon fa fa-laptop"></i>CONSULTAS</a>
                  </li>
                    <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user-md"></i>Doctores</a>
                     <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-edit"></i><a href="{{route ('altadoctor')}}">Registro</a></li>
                       <li><i class="fa fa-th-list"></i><a href="{{route ('mostrardoctores')}}">Consulta</a></li>
                     </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Pacientes</a>
                     <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-edit"></i><a href="{{route ('altapaciente')}}">Registro</a></li>
                       <li><i class="fa fa-th-list"></i><a href="{{route ('mostrarpacientes')}}">Consulta</a></li>
                     </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Citas</a>
                     <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-edit"></i><a href="{{route ('altacita')}}">Registro</a></li>
                       <li><i class="fa fa-th-list"></i><a href="{{route ('mostrarcitas')}}">Consulta</a></li>
                     </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-list-alt"></i>Recetas</a>
                     <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-edit"></i><a href="{{route ('altareceta')}}">Registro</a></li>
                       <li><i class="fa fa-th-list"></i><a href="{{route ('mostrarrecetas')}}">Consulta</a></li>
                     </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-h-square"></i>Operaciones</a>
                     <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-edit"></i><a href="{{route ('altaoperacion')}}">Registro</a></li>
                       <li><i class="fa fa-th-list"></i><a href="{{route ('mostraroperacion')}}">Consulta</a></li>
                     </ul>
                    </li>
                     <li class="menu-item-has-children dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bookmark"></i>Especialidades</a>
                     <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-edit"></i><a href="{{route ('altaespecialidad')}}">Registro</a></li>
                       <li><i class="fa fa-th-list"></i><a href="{{route ('mostrarespecialidades')}}">Consulta</a></li>
                     </ul>
                    </li>
                    <li>
                        <a href="{{route ('mostrarconsulta')}}"><i class="menu-icon fa fa-file"></i>Historial de consultas</a>
                    </li> 
                </ul>
            </div>
        </nav>
    </aside>
<!-----------------------------------Termina Barra del menu de Navegacion------------------------------->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="./"><img src="{{ asset('images/logo.png')}}" alt="Logo"></a>
                    <a class="navbar-brand hidden" href="./"><img src="{{ asset('images/logo2.png')}}" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control"  class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="{{ asset('images/admin.jpg')}}" alt="User Avatar">
                        </a>
                        <div class="user-menu dropdown-menu">
                             <a class="nav-link"><i class="fa fa-user"></i>{{ Auth::user()->name }}</a>
                            <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                             <i class="fa fa-power-off"></i>{{ __('Cerrar Sesión') }}</a>
                             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </header>
        <!-- /#header -->
        <!-- Content -->
<!-------------COMIENZA ENCABEZADO DE TITULO DE FORMULARIO------------------------------>
        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-header float-left">
                            <div class="page-title">
                              <div class="row">
                                <div>
                                <script>
                                    var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                                    var f=new Date();
                                    document.write("Fecha: " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
                                </script> 
                                </div>
                              </div>
                            </div>
                        </div>
                        <p style="margin-left:80%;">La clave de la cita es: 00{{$id_cita1}}</p>
                    </div>
                </div>
            </div>
        </div>
<!-------------TERMINA ENCABEZADO DE TITULO DE FORMULARIO------------------------------>
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
<!----------------------------------------------------------CONTENEDOR DE LAS LOS FORMULARIOS Y TABLAS---------------------------------------->
                            <div class="orders">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="table-stats order-table ov-h">
<!----------------------------------------------------------/**/*/START THE FORM*/*/*/---------------------------------------->
<form action = "{{route ('guardaconsulta')}}" method = "POST"  enctype='multipart/form-data'>
    {{csrf_field()}}
   <table class="table">
<!------------------------Cambio de input--------------------------------->
    <tr><td>Folio Consulta:</td>
    <td><input class="form-control" type='text' name ='id_con' value="{{$id_con1}}" readonly></td></tr>
<!------------------------Cambio de input--------------------------------->
    <tr><td>Fecha:</td>
    <td><input class="form-control col-lg-7" type='date' name ='fecha' placeholder="La fecha es requerida" id='fecha'></td>
<!------------------------Cambio de input--------------------------------->
    <td>
      @if($errors->first('hora'))
          <i> {{ $errors->first('hora') }} <br></i>
        @endif
      Hora:</td><td><input class="form-control col-lg-5" type = 'time' name = 'hora' placeholder="La hora es requerida" id="hora" value="{{old('hora')}}"></td></tr>
<!------------------------Cambio de input--------------------------------->
<tr><td>
  Doctor:</td><td>
  <select class="form-control col-lg-6" name = 'cat' id= 'cat'>
  <option selected disabled>Seleccione</option>
  @foreach($doctors as $doctor)
      <option value="{{$doctor->id_doc}}">{{$doctor->nombre}}</option>
  @endforeach
</select></td>
<!------------------------Cambio de input--------------------------------->
<td>
  Paciente:
</td><td>
  <div id="pro">
      <select id="selPac" class="form-control col-lg-6" disabled> 
      <option selected disabled>Seleccione</option>
      </select>
  </div></td></tr>
  <!------------------------Cambio de input--------------------------------->
<tr><td>
  Cita previa:</td><td>
  <input type="text" class="form-control" name="id_cita" value="{{$id_cita2}}" readonly ></td>
<!------------------------Cambio de input--------------------------------->
<tr> <td>
  Tipo de cita:
</td><td>
    <input type="radio" id="tip_cita1" name="tip_cita1" value="1" checked> Inicial
    <input style="margin-left:40%;" id="tip_cita2" name="tip_cita1" type="radio" name="tip_cita" id="radioseg" value="2"> Seguimiento
  </td></tr></div>
  <!------------------------Cambio de input--------------------------------->
  <tr id="alerg">
    <div>
    <td>Alergias: </td>
    <td>
      @if($errors->first('alergias'))
      <i> {{ $errors->first('alergias') }}<br></i>
      @endif
      <input class="form-control col-lg-12" type="text" name="alergias" placeholder="Alergias" value="sin registro"></td>
  </div>
  </tr>
    <!------------------------Cambio de input--------------------------------->
  <tr id="sitma">
    <div>
    <td>Sintomas: </td>
    <td>
      @if($errors->first('sintomas'))
      <i> {{ $errors->first('sintomas') }}<br></i>
      @endif
      <input class="form-control col-lg-12" type="text" name="sintomas" placeholder="Sintoma" value="sin registro"></td>
  </div>
  </tr>
    <!------------------------Cambio de input--------------------------------->
    <tr>
      <div id="dolor">
      <td>Dolor\Molestia: </td>
      <td>
        @if($errors->first('dolor'))
        <i> {{ $errors->first('dolor') }}<br></i>
        @endif
        <input class="form-control col-lg-12" type="text" name="dolor" placeholder="Molestias" value="{{old('dolor')}}"></td>
    </div>
    </tr>
    <!------------------------Cambio de input--------------------------------->
        <tr>
          <div id="medica">
          <td>Medicamento: </td>
          <td>
            @if($errors->first('medicamento'))
            <i> {{ $errors->first('medicamento') }}<br></i>
            @endif
            <input class="form-control col-lg-7" type="text" name="medicamento" placeholder="Medicamento" value="{{old('medicamento')}}"></td>
        </div>
        
      <!------------------------Cambio de input--------------------------------->
          
            <div id="docss">
            <td>Docis: </td>
            <td>
              @if($errors->first('docis'))
              <i> {{ $errors->first('docis')}}<br></i>
              @endif
              <input class="form-control col-lg-5" type="number" name="docis" min="0" max="90" placeholder="0" value="{{old('docis')}}"></td>
          </div>
          </tr>
            <!------------------------Cambio de input--------------------------------->
    <tr>
      <div id="diasmed">
      <td>Dias: </td>
      <td>
        @if($errors->first('dias'))
        <i> {{ $errors->first('dias') }}<br></i>
        @endif
        <input class="form-control" type="number" min="0" max="15" name="dias" placeholder="0" value="{{old('dias')}}"></td>
    </div>
    </tr>
    <!------------------------Cambio de input--------------------------------->
      <tr>
        <div id="dscrip">
        <td>Descripción: </td>
        <td>
          @if($errors->first('descripcion'))
          <i> {{ $errors->first('descripcion') }}<br></i>
          @endif
          <textarea class="form-control" style="width:100%;" name="descripcion" type="textarea" placeholder="Descripción..." value="{{old('descripcion')}}"></textarea></td>
      </div>
      </tr>
      <!------------------------Cambio de input--------------------------------->
    <tr><td colspan=2 ><center><input class="btn btn-lg btn-info" id="save" type ='submit' value = 'Guardar'></center></td></tr>
   </form>
<div id="tabload">
  <table id="datah" class="table">
  </table>
</div>
<!----------------------------------------------------------/**/*/TERMINA FORM*/*/*/---------------------------------------->
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
                    
<!----------------------------------------------------------/**/*/*/*/*/---------------------------------------->
    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="assets/js/main.js"></script>
   <script>
    // $(document).ready(function(){
    //   $("#seguimiento").hide();
    //   $("#pro").click(function(){
    //     $("#seguimiento").show();
    //   });
    // });
  </script> 
  <script>
      $(document).ready(function(){
      $("#datah").hide();
      // $("#tabseg").hide();
      $("input[name=tip_cita1]").change(function(){
      switch($('input:radio[name=tip_cita1]:checked').val()){
      case '1':
      $("#sitma").show();
      $("#alerg").show();
      $("#dolor").hide();
      $("#medica").hide();
      $("#docss").hide();
      $("#datah").hide();
      break;
      case '2':
      $("#sitma").hide();
      $("#alerg").hide();
      $("#dolor").show();
      $("#medica").show();
      $("#docss").show();
      $("#datah").show();
      break;
      }		
     });
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
      $( "#cat" )
      .change(function() {
        var dato;
        if ($('#cat').val()) {          
          $.get('{{route('pntte')}}' + '?id='  +this.options[this.selectedIndex].value,
            function(data, status){             
              if (data) {
                $("#selPac").empty().append("<option selected disabled>Seleccione</option>").prop("disabled", false);
                for (let i = 0; i < data.length; i++) {
                  $("#selPac").append("<option value="+data[i].idp+">"+data[i].nombre+" "+data[i].apellido1+"</option>");
                }                
              }else{

              }
            }
          );
          var id;
          $("#selPac").change(function() { 
              id = $('#selPac').val();
              if ($('#selPac').val()) {
                $.get('{{route('evalCita')}}' + '?id='  +this.options[this.selectedIndex].value,function(data, status){
                  dato = data;
                }).done(
                  function(){
                    var count = dato[0].Dato;
                    var valor;
                    if (count == 0) {
                      $("#tip_cita1").prop("checked", true);
                      $("#tip_cita2").prop("disabled", true);
                      $("#tip_cita1").prop("disabled", false);
                      valor = $('input:radio[name=tip_cita1]:checked').val();
                      if (valor == 1) {
                        $("#sitma").show();
                        $("#alerg").show();
                        $("#dolor").hide();
                        $("#medica").hide();
                        $("#docss").hide();
                        $("#datah").hide();
                      }              
                    }
                    if (count > 0) {
                      $("#tip_cita2").prop("checked", true);
                      $("#tip_cita1").prop("disabled", true);
                      $("#tip_cita2").prop("disabled", false);
                      valor = $('input:radio[name=tip_cita1]:checked').val();
                      if (valor == 2) {
                        $("#sitma").hide();
                        $("#alerg").hide();
                        $("#dolor").show();
                        $("#medica").show();
                        $("#docss").show();
                        $("#datah").show();
                      }
                    }
                  }
                );
                $.get('{{route('tablep')}}'+'?id='+id,function(data){
                    $('#tabload').empty().append(data);
                  }
                );
              }
            }
          );

        }        
      })
      .trigger( "change" );
        // $("#seg").click(function() {
        // $("#tabload").load('{{route('tablep')}}' + '?id='  +this.options[this.selectedIndex].value) ;
        // });
        $("#save").click(function() {
          alert("Por el momento se esta desarrollando este modulo");
        });      
      });
  </script>

<!------------------------------------------------------ Borrad de aqui ----------------------------------------------- -->
</body>
</html>
