<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\pacientes;
use App\Estados;
use App\Municipios;
class PacienteController extends Controller
{
    public function create()
    {
		/*______ este es el auto increment_______*/
		 $clavequesigue = pacientes::
			orderBy('idp','desc')
				 ->take(1)->get();
			 $idps = $clavequesigue[0]->idp+1;
				/*_______esta es la nueva variable de el modelo para aplicar en el combo__________*/
			 $pacientes = pacientes::all();
			 $estados = Estados::all();
			 $municipios = Municipios::all();
             if (Auth::check()) {
	        return view('altapaciente')
	        ->with('idps',$idps)
			->with('estados',$estados)
            ->with('municipios',$municipios);
        }
        else
         return view('auth.login');
            
            
}
// function cat(Request $request)
// {
//      $id = $request->get('id'); 
//      $municipios = municipios::where('id_edo','=',$id)->get();
//      return view ('municipiosform',compact('municipios'));
// } 
public function guardapaciente(Request $request)
{
	$idp=$request->idp;
		$nombre=$request->nombre;
		$apellido1=$request->apellido1;
		$apellido2=$request->apellido2;
		$edad=$request->edad;
		$sexo=$request->sexo;
		$telefono=$request->telefono;
		$id_edo=$request->id_edo;
        $id_mun=$request->id_mun;
        $calle=$request->calle;
        $numcalle=$request->numcalle;
        $localidad=$request->localidad;
        
		$this->validate($request,[
	  'idp'=>'required|numeric',
 		'nombre'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
 		'apellido1'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
 		'apellido2'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
		 'edad'=>'required|integer|min:18',
         'telefono'=>'required|regex:/^[0-9]{10}$/',
         'calle'=>'required|regex:/^[A-Z,a-z, ]+$/',
		 'numcalle'=>'required|regex:/^[0-9,0-9 ]+$/',
		 'localidad'=>'required|regex:/^[A-Z,a-z, ]+$/',
		 'archivo'=>'image|mimes:gif,jpeg,jpg,png'
		]);
		$file = $request->file('archivo');
		if($file!= "")
		{
	$ldate = date('Ymd_His_');
	$img = $file->getClientOriginalName();
	$img2 = $ldate.$img;
	\Storage::disk('local')->put($img2, \File::get($file));
		}
		else
		{
		 $img2 = 'sinfoto.jpg';
	 }
/*OOOOJOOOOO FALTA EL STORE DE LA DATABASE         altapaciente           */
	 $result=\DB::select('CALL altapaciente(?,?,?,?,?,?,?,?,?,?,?,?,?)',
	 [$idp,$nombre,$apellido1,$apellido2,$edad,$sexo,$telefono,$id_edo,$id_mun,$calle,$numcalle,$localidad,$img2]);
	$proceso = "Alta del paciente";
	$mensaje ="El paciente $nombre ha sido dado de alta";
	return view ('resultado')
	->with('proceso',$proceso)
	->with('mensaje',$mensaje);
 }
    public function mostrarpacientes(Request $request)
    {
        // $resultado=\DB::select("SELECT vd.idp,vd.nombre,vd.apellido1,vd.apellido2,vd.edad,vd.sexo,
        // vd.edad,vd.telefono,c.nombre,c.nombre,vd.calle,vd.numcalle,vd.localidad,vd.archivo
        // AS res1
        // FROM pacientes AS vd
        // INNER JOIN estados AS c ON c.idp = vd.id_edo
        // WHERE vd.idv= ?",[$request->idv]);
        $resultado=\DB::select("SELECT vd.idp,vd.nombre,vd.apellido1,vd.apellido2,vd.edad,vd.sexo,
        vd.edad,vd.telefono,vd.id_edo,vd.id_mun,vd.calle,vd.numcalle,vd.localidad,vd.archivo,
        c.nombre as catego,
        t.nombre as catego2
        FROM pacientes AS vd
        INNER JOIN estados AS c ON c.id_edo = vd.id_edo
        INNER JOIN municipios AS t ON t.id_mun = vd.id_mun");
        if (Auth::check()) {
         return view ('mostrarpacientes')
         ->with('resultado',$resultado);
        }
        else
         return view('auth.login');
    
    }
    public function borrarpaciente($idp)
    {
            pacientes::find($idp)->delete();
            $proceso = "Eliminacion del paciente";
            $mensaje ="El paciente con clave $idp ha sido eliminado :(";
            return view ('resultado')
            ->with('proceso',$proceso)
            ->with('mensaje',$mensaje);
    }
    public function modificapaciente($idp)
{
$pacientes= pacientes::where('idp',$idp)->get();
$id_edo = $pacientes[0]->id_edo;
$nombrestado = estados::where('id_edo',$id_edo)->get();
$estados = estados::where('id_edo','<>',$id_edo)->get();
$id_mun = $pacientes[0]->id_mun;
$nombremunicipio = municipios::where('id_mun',$id_mun)->get();
$municipios = municipios::where('id_mun','<>',$id_mun)->get();
if (Auth::check()) {
return view('modificapaciente')
->with('pacientes',$pacientes[0])
->with('estados',$estados)
->with('id_edo',$id_edo)
->with('nombrestado',$nombrestado[0])
->with('municipios',$municipios)
->with('id_mun',$id_mun)
->with('nombremunicipio',$nombremunicipio[0]);
}
else
 return view('auth.login');
}

public function edicionpaciente(Request $request)
{
    $idp=$request->idp;
    $nombre=$request->nombre;
    $apellido1=$request->apellido1;
    $apellido2=$request->apellido2;
    $edad=$request->edad;
    $sexo=$request->sexo;
    $telefono=$request->telefono;
    $id_edo=$request->id_edo;
    $id_mun=$request->id_mun;
    $calle=$request->calle;
    $numcalle=$request->numcalle;
    $localidad=$request->localidad;
    
    $this->validate($request,[
  'idp'=>'required|numeric',
     'nombre'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
     'apellido1'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
     'apellido2'=>'required|regex:/^[A-Z][A-Z,a-z, ]+$/',
     'edad'=>'required|integer|min:18',
     'telefono'=>'required|regex:/^[0-9]{10}$/',
     'calle'=>'required|regex:/^[A-Z,a-z, ]+$/',
     'numcalle'=>'required|regex:/^[0-9,0-9 ]+$/',
     'localidad'=>'required|regex:/^[A-Z,a-z, ]+$/',
     'archivo'=>'image|mimes:gif,jpeg,jpg,png'
    ]);
    $file = $request->file('archivo');
    if($file!= "")
    {
        $ldate = date('Ymd_His_');
        $img = $file->getClientOriginalName();
        $img2 = $ldate.$img;
        \Storage::disk('archivos')->put($img2, \File::get($file));
    }
    else
    {
     $img2 = 'sinfoto.jpg';
    }
    $result=\DB::select('select modificapaciente(?,?,?,?,?,?,?,?,?,?,?,?,?)',
    [$idp,$nombre,$apellido1,$apellido2,$edad,$sexo,$telefono,$id_edo,$id_mun,$calle,$numcalle,$localidad,$img2]);
	$proceso = "Modificación de datos del paciente";
		$mensaje ="El paciente con la clave $idp y el nombre $nombre ha sido modificado correctamente";
		return view ('resultado')
		->with('proceso',$proceso)
		->with('mensaje',$mensaje);
    }
}
