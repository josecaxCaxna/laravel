<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Estados extends Migration
{
    public function up()
    {
      Schema::create('estados', function (Blueprint $table) {
         $table->increments('id_edo');
         $table->string('nombre',25);
   });
   }
    public function down()
    {
  Schema::drop('estados');
    }
}
