<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas', function (Blueprint $table) {
            $table->increments('id_cita');
            $table->string('f_agenda',25);
            $table->string('f_asistencia',25);
            $table->integer('id_doc')->unsigned();
            $table->foreign('id_doc')->references('id_doc')->on('doctors');
            $table->integer('idp')->unsigned();
            $table->foreign('idp')->references('idp')->on('pacientes');        
            $table->string('notac',25);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas');
    }
}
