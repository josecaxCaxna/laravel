<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Municipios extends Migration
{
    public function up()
    {
      Schema::create('municipios', function (Blueprint $table) {

        $table->integer('id_edo')->unsigned();
        $table->foreign('id_edo')->references('id_edo')->on('estados');

         $table->increments('id_mun');
         $table->string('nombre',25);
       });
       }
    public function down()
    {
    Schema::drop('municipios');
    }
}
