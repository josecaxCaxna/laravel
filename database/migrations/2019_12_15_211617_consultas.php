<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Consultas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
      Schema::create('consultas', function (Blueprint $table) {
        $table->increments('id_con');
        $table->date('fecha');
        $table->time('hora');
        $table->string('tip_cita',25);
        $table->string('sintomas',255);
        /*______________aqui se otorga la id de la tabla de donde se extraera
         la llave primaria que en este caso seria foranea haciendo referencia a
         la tabla de donde se pedira ________________-*/
         $table->integer('id_cita')->unsigned();
         $table->foreign('id_cita')->references('id_cita')->on('citas');
        $table->integer('id_doc')->unsigned();
        $table->foreign('id_doc')->references('id_doc')->on('doctors');
        $table->integer('idp')->unsigned();
        $table->foreign('idp')->references('idp')->on('pacientes');
        $table->string('observacion',255);
        $table->string('medicamento',25);
        $table->string('descripcion',255);
        $table->rememberToken();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultas');
    }
}
