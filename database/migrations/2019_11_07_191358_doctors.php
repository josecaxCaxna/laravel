<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Doctors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id_doc');
            $table->string('nombre',25);
            $table->string('apellido1',25);
            $table->string('apellido2',25);
            $table->string('edad',25);
            $table->string('sexo',25);
/*______________aqui se otorga la id de la tabla de donde se extraera
    la llave primaria que en este caso seria foranea haciendo referencia a
            la tabla de donde se pedira ________________-*/
            $table->integer('id_esp')->unsigned();
            $table->foreign('id_esp')->references('id_esp')->on('especialidades');
            $table->string('telefono',25);
 /*______________aqui se otorga la id de la tabla de donde se extraera
             la llave primaria que en este caso seria foranea haciendo referencia a
                            la tabla de donde se pedira ________________-*/
            $table->integer('id_edo')->unsigned();
            $table->foreign('id_edo')->references('id_edo')->on('estados');
            $table->integer('id_mun')->unsigned();
            $table->foreign('id_mun')->references('id_mun')->on('municipios');
            $table->string('calle',25);
            $table->string('numcalle',25);
            $table->string('localidad',25);
            $table->string('archivo',255); 
            $table->rememberToken();  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
